import sys
import logging

import unittest
from fuzzyhash.fuzzyhash import SSDEEPCheck, SSDEEPBody
from io import BytesIO
from fuglu.stringencode import force_uString
from fuglu.shared import Suspect, FuConfigParser
from testing.storedmails import mailsdir
from os.path import join

# test mails to import
from testing.storedmails import mail_helloWorld, mail_fakeEXEinrar, mail_fakeEXEinzip
from unittest.mock import patch
from unittest.mock import MagicMock



def get_session_mock(*a, **kw):
    # mock the session object
    m = MagicMock()
    # and the execute function of the session object
    m.execute.return_value = [["3:hMCEu3Q3Fx:huuQ3Fx", "fakeEXE.exe"]]
    return m

def setup_module():
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    root.addHandler(handler)


class FuzzyhashPlugintest(unittest.TestCase):
    """
    Test fuzzyhash plugin
    - SSDEEPcheck which will hash the files in attached archives
    - SSDEEPBody which will hash the biggest text part found in the mail body
    """

    @patch('fuzzyhash.fuzzyhash.get_session', side_effect=get_session_mock)
    def test_zipattachment(self,get_session_func):
        """
        Test if exe is found in zip - attachment and hashed correctly
        """

        myclass = self.__class__.__name__
        functionNameAsString = sys._getframe().f_code.co_name
        loggername = "%s.%s" % (myclass,functionNameAsString)
        logger = logging.getLogger(loggername)

        logger.debug("Config parser")
        confdefaults = r"""
[test]
dbconnectstring=asdf
maxsize=1000000000000
minattachmentsize=0
maxattachmentsize=1000000000000
archivecontentmaxsize=1000000000000
threshold=75
virusaction=DEFAULTVIRUSACTION
problemaction=DEFER
rejectmessage=threat detected: ${virusname}
filenamesrgx=\.(exe|scr|com|pif)$
messagetemplate=zeroday.SSD/sc${score}
"""
        conf = FuConfigParser()
        conf.read_string(force_uString(confdefaults))

        logger = logging.getLogger(loggername)
        logger.debug("Read file content")
        filecontent = BytesIO(mail_fakeEXEinzip).read()

        s = SSDEEPCheck(conf, 'test')
        viruses_out = s.scan_stream(filecontent)
        logger.debug("Output from scan_stream for viruses: %s" % str(viruses_out))
        self.assertEqual({'fakeEXE.zip->fakeEXE.exe': 'zeroday.SSD/sc100'},viruses_out)

    @patch('fuzzyhash.fuzzyhash.get_session', side_effect=get_session_mock)
    def test_rarattachment(self,get_session_func):
        """
        Test if exe is found in rar - attachment and hashed correctly
        """
        #--------#
        # logger #
        #--------#

        myclass = self.__class__.__name__
        functionNameAsString = sys._getframe().f_code.co_name
        loggername = "%s.%s" % (myclass,functionNameAsString)
        logger = logging.getLogger(loggername)

        logger.debug("Config parser")
        confdefaults = r"""
[test]
dbconnectstring=asdf
maxsize=1000000000000
minattachmentsize=0
maxattachmentsize=1000000000000
archivecontentmaxsize=1000000000000
threshold=75
virusaction=DEFAULTVIRUSACTION
problemaction=DEFER
rejectmessage=threat detected: ${virusname}
filenamesrgx=\.(exe|scr|com|pif)$
messagetemplate=zeroday.SSD/sc${score}
"""
        conf = FuConfigParser()
        conf.read_string(force_uString(confdefaults))

        logger.debug("Read file content")
        filecontent = BytesIO(mail_fakeEXEinrar).read()

        s = SSDEEPCheck(conf, 'test')
        viruses_out = s.scan_stream(filecontent)
        logger.debug("Output from scan_stream for viruses: %s" % str(viruses_out))
        self.assertEqual({'fakeEXE.rar->fakeEXE.exe': 'zeroday.SSD/sc100'},viruses_out)
    
    
    def _mock_get_hashes(self):
        return {"3:8hvNZREBkbRAd8n:8hv5":"Hello world!"}
    
    def test_examine(self):
        """
        Test if text in mail body is detected and hashed correctly


        Note:
        Do not patch all the system commands to reload the database file like:
            @patch('os.stat', side_effect=mock_os_stat)
            @patch('os.path.exists', return_value=True)
            @patch('fuzzyhash.open', new_callable=mock_open, read_data="3:8hvNZREBkbRAd8n:8hv5\tHello world!")
            def test_examine(self,openfile_func,os_path_exists,os_stat):
                ...
        because Suspect.size will become a mocked object too
        (Suspect calls internally self.size = os.path.getsize(tempfile) but I don't know exactly why this gets mocked)
        """

        myclass = self.__class__.__name__
        functionNameAsString = sys._getframe().f_code.co_name
        loggername = "%s.%s" % (myclass,functionNameAsString)
        logger = logging.getLogger(loggername)

        logger.debug("Config parser")
        conf = FuConfigParser()
        confdefaults = r"""
[test]
#database url
hashfile = /tmp/ssdeephashes.txt

# "maximum message size, larger messages will not be scanned.
maxsize = 22000000 

# minimum text part size to be considered for ssdeep
mintextpartsize = 1

# maximum text part size to be considered for ssdeep
maxtextpartsize = 22000000

# loghash: just output hash, 
# logscore: log hash and current score
# header: write header if hit is detected, 
# write: append to the database(trap server)',
mode= header

# ssdeep threshold. mode write: append to db if below. other modes: perform action if above'
threshold = 75

virusaction=DEFAULTVIRUSACTION
problemaction=DEFER
rejectmessage=threat detected: ${virusname}
filenamesrgx=\.(exe|scr|com|pif)$
messagetemplate=zeroday.SSD/sc${score}
"""
        conf.read_string(force_uString(confdefaults))

        logger.debug("Read file content")
        filecontent = BytesIO(mail_helloWorld).read()

        suspect = Suspect("from@fuglu.test","to@fuglu.test","/dev/null")
        suspect.set_source(filecontent)

        # at this point there should be no ssdeep header yet
        self.assertEqual(None,suspect.addheaders.get('X-SSDEEP-Score'))

        s = SSDEEPBody(conf,'test')

        # create a mock for loading the db and set known hashes by hand

        s._get_hashes = self._mock_get_hashes

        out = s.examine(suspect)
        logger.debug("output = %s"%out)

        # there should be a header 'X-SSDEEP-Score' with value '100' because
        # this is a 100% match
        self.assertNotEqual(None,suspect.addheaders.get('X-SSDEEP-Score'))
        self.assertEqual('100',suspect.addheaders['X-SSDEEP-Score'])


class FuzzyhashTestScanStream(unittest.TestCase):

    def setUp(self):
        confdefaults = r"""
[test]
dbconnectstring=asdf
maxsize=1000000000000
minattachmentsize=0
maxattachmentsize=1000000000000
archivecontentmaxsize=1000000000000
threshold=75
virusaction=DEFAULTVIRUSACTION
problemaction=DEFER
rejectmessage=threat detected: ${virusname}
filenamesrgx=\.(exe|scr|com|pif)$
messagetemplate=zeroday.SSD/sc${score}
"""
        conf = FuConfigParser()
        conf.read_string(force_uString(confdefaults))

        self.ssdeep = SSDEEPCheck(conf, 'test')

    @patch('fuzzyhash.fuzzyhash.get_session', side_effect=get_session_mock)
    def test_non_brokenbase64(self, get_session_func):
        """Original case with correct file, from which the two broken cases below are derived"""
        filecontent = open(join(*[mailsdir, "inline_image.eml"]), "rb").read()
        viruses_out = self.ssdeep.scan_stream(filecontent)
        print("Output from scan_stream for viruses: %s" % str(viruses_out))
        self.assertIsNone(viruses_out)

    @patch('fuzzyhash.fuzzyhash.get_session', side_effect=get_session_mock)
    def test_brokenbase64(self, get_session_func):
        """Test version of "inline_image.eml" suddenly ending in the middle of base64 encoding, should not
        create exception"""

        filecontent = open(join(*[mailsdir, "inline_image_broken.eml"]), "rb").read()
        viruses_out = self.ssdeep.scan_stream(filecontent)
        print("Output from scan_stream for viruses: %s" % str(viruses_out))
        self.assertIsNone(viruses_out)

    @patch('fuzzyhash.fuzzyhash.get_session', side_effect=get_session_mock)
    def test_brokenbase64_2(self, get_session_func):
        """Test version of "inline_image.eml" with a missing character in the base64 encoding, should not
        create exception"""
        filecontent = open(join(*[mailsdir, "inline_image_broken2.eml"]), "rb").read()
        viruses_out = self.ssdeep.scan_stream(filecontent)
        print("Output from scan_stream for viruses: %s" % str(viruses_out))
        self.assertIsNone(viruses_out)
