# -*- coding: UTF-8 -*-
from fuglu.shared import ScannerPlugin, DUNNO, DELETE, SuspectFilter
import os


class KillerRulePlugin(ScannerPlugin):
    """This plugins is similar to killer plugin except it only deletes a msg upon a matching rule (Suspect Filter).
It aims to be safer than killer plugin because without a match it does noting whereas killerplugin will kill any mail if the match in skippluginregex does not apply

The rulefile works similar to the archive plugin. As third column you have to provide an action which can be DELETE/delete or NO/no

"""

    def __init__(self, config, section=None):
        super().__init__(config, section)

        self.requiredvars = {
            'killrules': {
                'default': '/etc/fuglu/killrules.regex',
                'description': 'IMAP copy suspectFilter File',
            },
        }
        self.filter = None
        self.logger = self._logger()

    def examine(self, suspect):
        killrules = self.config.get(self.section, 'killrules')
        if killrules is None or killrules == "":
            return DUNNO

        if not os.path.exists(killrules):
            self._logger().error('killer rules file does not exist : %s' % killrules)
            return DUNNO

        if self.filter is None:
            self.filter = SuspectFilter(killrules)

        match, info = self.filter.matches(suspect, extended=True)
        if match:
            if info.arg is not None and info.arg.lower() == 'no':
                suspect.debug("Suspect matches kill plugin exception rule")
                self.logger.info(
                    """%s: Header %s matches kill plugin exception rule '%s' """ % (suspect.id, info.fieldname, info.pattern))
            else:
                if info.arg is None or info.arg.lower() != 'delete':
                    self.logger.error("Unknown action '%s' should be 'DELETE'" % info.arg)

                else:
                    if suspect.get_tag('debug'):
                        suspect.debug("Suspect matches kill rule (I would copy it if we weren't in debug mode)")
                    else:
                        self.logger.info("""%s: killer rule on '%s' matches '%s' --> DELETE""" % (suspect.id, info.fieldname, info.pattern))
                        return DELETE
        else:
            suspect.debug("No kill rule/exception rule applies to this message")
        return DUNNO

    def lint(self):
        allok = (self.check_config() and self.lint_action())
        return allok

    def lint_action(self):
        killrules = self.config.get(self.section, 'killrules')
        if killrules != '' and not os.path.exists(killrules):
            print("Kill rules file does not exist : %s" % killrules)
            return False
        sfilter = SuspectFilter(killrules)
        if len(sfilter.content) == 0:
            print("Empty rules file %s this plugin does nothing" % (killrules))
            return True
        for rule in sfilter.content:
            if rule.args.lower() != 'no' and rule.args.lower() != 'delete':
                print("Kill rule %s %s has unknown action %s" %(rule.fieldname, rule.pattern, rule.args))
                return False
        return True