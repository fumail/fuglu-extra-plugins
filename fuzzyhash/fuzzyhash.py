#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#   Copyright Fumail Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#
__version__ = '0.4'

import re
import sys
import time
import email
from email.header import decode_header
import traceback
import threading
import os
import logging

#This is not really used in fuglu, it's just a dummy implementation compatible with other fuglu AV scanners, so we can use it in external scripts easily
from fuglu.shared import AVScannerPlugin, SuspectFilter,DUNNO,FileList,apply_template,_SuspectTemplate
from fuglu.extensions.sql import get_session, text
from fuglu.extensions import fuzzyhashlib
from fuglu.stringencode import force_uString
from fuglu.lib.patchedemail import PatchedMessage
from fuglu.mailattach import Mailattachment_mgr, NoExtractInfo
from fuglu.plugins.antivirus import HashesFile

try:
    import ssdeep
    HAVE_SSDEEP = True
except ImportError:
    ssdeep = None
    HAVE_SSDEEP = False


class SSDEEPCheck(AVScannerPlugin):
    """Submit attachments in virus infected messages to atthash"""
    
    def __init__(self, config, section=None):
        super().__init__(config, section)
        
        self.requiredvars = {
            'dbconnectstring':{
                'default':'',
                'description': 'database url, e.g. mysql://root@localhost/ssdeep',
            },
            
            'dbsqlquery':{
                'default':"SELECT ssdeep,filename FROM hashinfo",
                'description': 'database query',
            },
            
            'dbfilepath':{
                'default': '',
                'description': 'database query',
            },
            
            'maxsize': {
                'default': '22000000',
                'description': "maximum message size, larger messages will not be scanned.  ",
            },

            'threshold':{
                'default':'75',
                'description': 'ssdeep lock threshold',
            },
            
            'minattachmentsize':{
                'default':'5',
                'description': 'minimum attachment size',
            },
            
            'maxattachmentsize':{
                'default':'5000000',
                'description': 'maximum attachment size',
            },
            
            'virusaction': {
                'default': 'DEFAULTVIRUSACTION',
                'description': "action if infection is detected (DUNNO, REJECT, DELETE)",
            },

            'archivecontentmaxsize': {
                'default': '5000000',
                'description': 'only extract and examine files up to this amount of (uncompressed) bytes',
            },

            'problemaction': {
                'default': 'DEFER',
                'description': "action if there is a problem (DUNNO, DEFER)",
            },

            'rejectmessage': {
                'default': 'threat detected: ${virusname}',
                'description': "reject message template if running in pre-queue mode and virusaction=REJECT",
            },
            
            'filenamesrgx': {
                'default': r'\.(exe|scr|com|pif)$',
                'description': 'regex of filenames to be hashed',
            },
            
            'virusnametemplate': {
                'default': 'zeroday.SSD/sc${score}',
                'description': 'template of virus name',
            },
        }
        self.logger = self._logger()
        self.hashes_file = None
        self.regex_cache = {}
        self.archive_passwords = {}
        self.enginename = 'ssdeep'

    def __str__(self):
        return "SSDEEP"

    def examine(self,suspect):
        if not HAVE_SSDEEP:
            return DUNNO
        if self._check_too_big(suspect):
            return DUNNO

        content = suspect.get_source()
        self.archive_passwords[suspect.id] = suspect.get_tag('archive_password', [])
        try:
            viruses = self.scan_stream(content, suspect.id)
            actioncode, message = self._virusreport(suspect, viruses)
            del self.archive_passwords[suspect.id]
            return actioncode, message
        except Exception as e:
            self.logger.warning(f'{suspect.id} Error encountered while checking ssdeep: {e.__class__.__name__}: {str(e)}')
            
        del self.archive_passwords[suspect.id]
        return self._problemcode()

    def scan_stream(self, content, suspectid='(n/a)', attachmentmanager=None):
        dr = dict()

        if attachmentmanager:
            # if sent in, use attachment manager since it has all information and files already
            # extracted and buffered
            attmanager = attachmentmanager
        else:
            # otherwise create the manager from the message
            try:
                msg = email.message_from_string(content, _class=PatchedMessage)
            except TypeError:
                msg = email.message_from_bytes(content, _class=PatchedMessage)
            attmanager = Mailattachment_mgr(msg, suspectid)

        for att in attmanager.get_objectlist():
            att_name = att.filename
            if not att.is_archive:  # no mime-type or filename regex matched the current attachment
                self.logger.debug(f'{suspectid} {att_name} skipping: not an archive')
                continue

            payload = att.buffer
            att_len = len(payload)
            
            self.logger.debug(f'{suspectid} check {att_name} with length={att_len}')
            if att_len < self.config.getint(self.section, 'minattachmentsize'):
                continue
            if att_len > self.config.getint(self.section, 'maxattachmentsize'):
                continue
            
            archive_passwords = self.archive_passwords.get(suspectid, [])
            if archive_passwords:
                att.archive_passwords = archive_passwords
            if att.filename_generated:
                # if filename was auto-generated, set to None to be consistent with old implementation
                att_name = None
            
            try:
                self.logger.debug(f'{suspectid} handle_archive')
                filename, score = self.handle_archive(att, suspectid)
                if filename is not None:
                    att_ascii_name = att.get_mangled_filename(keepending=True)
                    ext_ascii_name = att.mangle_filename(filename, keepending=True)
                    key = f'{att_ascii_name if att_name is not None else att_name}->{ext_ascii_name}'
                    template = _SuspectTemplate(self.config.get(self.section, 'messagetemplate'))
                    value = template.safe_substitute({'score':score, 'att_ascii_name':att_ascii_name, 'ext_ascii_name':ext_ascii_name})
                    dr[key] = value
                    return dr
            
            except Exception as e:
                message = str(e)
                if 'password' in message:
                    pass  # hide error message due to encrypted archive
                elif 'compression type' in message:
                    pass  # hide error message due to unsupported pkzip type 9 (deflate64) etc
                elif 'Bad magic number' in message:
                    pass  # hide errors from invalid zip files
                else:
                    #self.logger.error("could not handle attachment: ")
                    self.logger.debug(traceback.format_exc())
                    raise
        return None
    
    def _get_hashes_sql(self):
        session = get_session(self.config.get(self.section, 'dbconnectstring'))
        sqlquery = self.config.get(self.section, 'dbsqlquery')
        res=session.execute(text(sqlquery))
        known_hashes = {}
        for row in res:
            known_hashes[row[0]] = row[1]
        return known_hashes
    
    def _get_hashes_file(self, dbfile):
        hashes = {}
        if self.hashes_file is None:
            self.hashes_file = HashesFile(dbfile)
        if self.hashes_file is not None:
            hashes = self.hashes_file.get_list()
        return hashes
    
    def _get_hashes(self):
        hashes = {}
        if self.config.get(self.section, 'dbconnectstring'):
            hashes.update(self._get_hashes_sql())
        dbfile = self.config.get(self.section, 'dbfilepath')
        if dbfile and os.path.exists(dbfile):
            hashes.update(self._get_hashes_file(dbfile))
        return hashes

    def handle_archive(self, att, suspectid='(n/a)'):
        """Return the known filename with the hightest hitrate (above the configured threshold or None if no file reaches configured threshold"""

        att_name = att.filename
        att_ascii_name = att.get_mangled_filename(keepending=True)
        found_filename = None
        found_score = 0
        
        archivecontentmaxsize = self.config.getint(self.section, 'archivecontentmaxsize')
        regexstring = self.config.get(self.section, 'filenamesrgx')
        if regexstring in self.regex_cache:
            rgx = self.regex_cache[regexstring]
        else:
            rgx = re.compile(regexstring)
            self.regex_cache[regexstring] = rgx
        known_hashes = self._get_hashes()
        
        if att.archive_handle is None:
            self.logger.info(f'{suspectid} {att_name} invalid archive?')

        namelist = att.fileslist_archive

        for name in namelist:
            if not rgx.search(name):
                self.logger.debug(f'{suspectid} name: {name} not matching pattern: {rgx.pattern}')
                continue

            noextractinfo = NoExtractInfo()
            extracted_object = att.get_archive_obj(name, archivecontentmaxsize, noextractinfo=noextractinfo)
            if not extracted_object:
                for item in noextractinfo.get_filtered():
                    try:
                        filename, message = item
                        self.logger.info(f'{suspectid} file {name} in archive {att_ascii_name} failed to extract due to {message}')
                    except Exception as e:
                        self.logger.info(f'{suspectid} file {name} in archive {att_ascii_name} failed to extract due to {e.__class.__name}: {str(e)}')
                continue
            
            sshash = extracted_object.get_checksum('ssdeep')
            ext_ascii_name = extracted_object.get_mangled_filename(keepending=True)
            self.logger.debug(f'{suspectid} sshash={sshash} filename={ext_ascii_name}')

            for knownhash, filename in iter(known_hashes.items()):
                try:
                    score = ssdeep.compare(knownhash, sshash)
    
                    if score >= self.config.getint(self.section, 'threshold') and score > found_score:
                        found_score = score
                        found_filename = filename
                except ssdeep.InternalError as e:
                    self.logger.error(f'{suspectid} failed to compare ssdeep hashes. knownhash={knownhash} sshash={sshash} error={str(e)}')

        return found_filename, found_score
    
    

class SSDEEPBody(SSDEEPCheck):
    """Build """

    def __init__(self, config, section=None):
        super().__init__(config, section)
        self.filter=SuspectFilter(None)
        self.requiredvars.update({
            'maxsize': {
                'default': '22000000',
                'description': "maximum message size, larger messages will not be scanned.  ",
            },

            'mintextpartsize': {
                'default': '100',
                'description': "minimum text part size to be considered for ssdeep ",
            },
             'maxtextpartsize': {
                'default': '22000000',
                'description': "maximum text part size to be considered for ssdeep ",
            },
            'mode':{
                'default':'loghash',
                'description': 'loghash: just output hash, logscore: log hash and current score, header: write header if hit is detected, write: append to the database(trap server)',
            },
            'threshold':{
                'default':'75',
                'description': 'ssdeep threshold. mode write: append to db if below. other modes: perform action if above',
            },

        })
        self.logger = self._logger()
        self.lock=threading.Lock()


    def __str__(self):
        return "SSDEEP Body"


    def examine(self,suspect):
        if not HAVE_SSDEEP:
            return DUNNO
        if suspect.size > self.config.getint(self.section, 'maxsize'):
            self.logger.info(f'{suspect.id} Not scanning - message too big')
            return DUNNO

        msg = suspect.get_message_rep()
        textparts = self.filter.get_decoded_textparts(suspect)

        self.logger.debug(f"{suspect.id} Number of text parts in mail: {len(textparts)}")
        #fallback to full body if no declared text part
        if len(textparts)==0:
            textparts=[msg.get_payload(decode=True)]
            self.logger.debug(f"{suspect.id} No text part, read the full body")

        #find the largest text part within the configured bounds
        candidate=None
        largest=0
        for part in textparts:
            if part is None:
                continue
            partlen=len(part)
            #self.logger.info("prtlen %s"%partlen)
            if partlen<self.config.getint(self.section,'mintextpartsize') or partlen>self.config.getint(self.section,'maxtextpartsize'):
                continue
            if partlen<=largest:
                continue
            candidate=part
            largest=partlen


        if candidate is None:
            self.logger.info(f"{suspect.id} SSDEEP body check: no suitable textpart found")
            return DUNNO
        sshash=ssdeep.hash(candidate)
        filename='messagebody'
        subject=msg.get('subject',None)
        if subject is not None:
            try:
                filename="".join(x[0] for x in decode_header(subject))
                filename = "".join([x for x in filename if ord(x) < 128])
                if len(filename)<1:
                    filename='messagebody'
            except Exception:
                pass


        mode = self.config.get(self.section,'mode')
        if mode=='loghash':
            self.logger.info(f"{suspect.id} SSDEEP body check: text part={largest} bytes, ssdeep={sshash} identifier={filename}")
            return DUNNO
        
        found_score=0
        hashes = self._get_hashes()
        for knownhash,knownfilename in iter(hashes.items()):
            score=ssdeep.compare(knownhash,sshash)
            #self.logger.info('score against %s = %s'%(knownhash,score))
            if score>found_score:
                found_score=score
            if score>99:
                break

        self.logger.info(f"{suspect.id} SSDEEP body check: score={found_score} text part={largest} bytes, ssdeep={sshash} identifier={filename}")
        suspect.add_header('X-SSDEEP-Score',str(found_score))
        if mode=='logscore':
            return DUNNO

        threshold = self.config.getint(self.section,'threshold')
        if mode=='write' and found_score<=threshold:
            self.write(sshash,filename)
        return DUNNO

    def write(self,sshash,filename):
        hashfile = self.config.get(self.section,'dbfilepath')
        gotlock=self.lock.acquire(False)
        if gotlock:
            with open(hashfile, 'a') as fp:
                fp.write(f'{sshash}\t{filename}\n')
            self.lock.release()


if __name__ == '__main__':
    try:
        from unittest.mock import patch
        from unittest.mock import MagicMock
    except ImportError:
        from mock import patch
        from mock import MagicMock


    def get_session_mock(*a, **kw):
        # mock the session object
        m = MagicMock()
        # and the execute function of the session object
        m.execute.return_value = []
        return m


    #--------#
    # logger #
    #--------#
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    root.addHandler(handler)

    path_filename=os.path.abspath(__file__)
    path, fn = os.path.split(path_filename)
    loggername = "(%s) %s" % (fn,__name__)
    logger = logging.getLogger(loggername)

    if len(sys.argv[1:]) == 0:
        print('no file name specified')
        sys.exit(1)

    with open(sys.argv[1]) as f:
        filecontent = f.read()

    from fuglu.shared import FuConfigParser
    conf = FuConfigParser()
    confdefaults = b"""
[test]
dbconnectstring=
minattachmentsize=0
maxattachmentsize=1000000000000
archivecontentmaxsize=1000000000000
"""
    conf.read_string(force_uString(confdefaults))

    s = SSDEEPCheck(conf, 'test')
    with patch('__main__.get_session', side_effect=get_session_mock):
        logger.debug("Output from scan_stream for viruses: %s" % str(s.scan_stream(filecontent)))

